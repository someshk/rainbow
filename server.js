
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes');
  
// someshk 
var uuid = require('node-uuid');
var Home = require('./home');
var azure = require('azure');

var app = module.exports = express.createServer();

// someshk
var client = azure.createTableService("618ddd3592814af7b", 'Q/U9uGr2Y6IIbtY8893TkgKlnjFyUoJptsYcFlHsRdrByEBs/AX8HjwJRkPCnpiSuXQAsTMOlnLmAk3CheZknA==');

// someshk - Create table
// table creation
/* client.createTableIfNotExists("tasks", function (res, created) {
  if (created) {
    var item = {
      name: 'Add readonly todo list',
      category: 'Site work',
      date: '12/01/2011',
      RowKey: uuid(),
      PartitionKey: 'partition1',
      completed: false
    };

    client.insertEntity("tasks", item, null, function () {
      setupApplication();
    });
  } else {
    setupApplication();
  }
});*/

//table creation
client.createTableIfNotExists('tasks', function(error){
    if(error){
        throw error;
    }

    var item = {
        name: 'Add readonly task list',
        category: 'Site work',
        date: '12/01/2011',
        RowKey: uuid(),
        PartitionKey: 'partition1',
        completed: false
    };

    client.insertEntity('tasks', item, function(){});

});


// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);

// someshk- Setup Routes
var home = new Home(client);
app.get('/home', home.showItems.bind(home));
app.post('/home/newitem', home.newItem.bind(home));

app.listen(3022, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
