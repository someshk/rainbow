var azure=require('azure');
var uuid = require('node-uuid');

module.exports = Home;

function Home (client) {
    this.client = client;
};

Home.prototype = {
    showItems: function (req, res) {
        var self = this;
        this.getItems(false, function (resp, tasklist) {
            if (!tasklist) {
                tasklist = [];
            }     
            self.showResults(res, tasklist);
        });
    },

    getItems: function (allItems, callback) {
        var query = azure.TableQuery
            .select()
            .from('tasks');
  
        if (!allItems) {
            query = query.where('completed eq ?', 'false');
        }
        this.client.queryEntities(query, callback);
     },

     showResults: function (res, tasklist) {
        res.render('home', { 
            title: 'Todo list', 
            layout: false, 
            tasklist: tasklist });
     },
      newItem: function (req, res) {
       var self = this;
       var createItem = function (resp, tasklist) {
           if (!tasklist) {
               tasklist = [];
           }

           var count = tasklist.length;

           var item = req.body.item;
           item.RowKey = uuid();
           item.PartitionKey = 'partition1';
           item.completed = false;

           self.client.insertEntity('tasks', item, function (error) {
               if(error){  
                   throw error;
               }
               self.showItems(req, res);
           });
       };

       this.getItems(true, createItem);
   },
};